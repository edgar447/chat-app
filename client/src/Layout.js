import React, { Component } from 'react';
import logo from './logo.svg';
import './Styles/Layout.css';
import NavBar from "./NavBar"
import Login_Register from "./Login_Register";
import Chat from "./Chat";
import openSocket from 'socket.io-client';
import axios from 'axios';

class Layout extends Component {
constructor(props){
  super(props);

  this.showChat = this.showChat.bind(this);
  this.showLogin = this.showLogin.bind(this);
  this.showRegister = this.showRegister.bind(this);
  this.createSocketconnection = this.createSocketconnection.bind(this);



}
state ={
  socket:null,
  onlineUsers:[],
  login : true,
  register: false,
  chat :false 
}

createSocketconnection(username){
 // this.setState({onlineUserSelected : onlineUser});
  console.log("looooged in");
//  this.props.userSelection(onlineUser);            
var socket = openSocket('http://localhost:3001');
this.setState({Socket:socket}); // console.log(onlineUser);

socket.on('connect', () => {
  console.log(socket.id); // 'G5p5...'

axios.post('http://localhost:3001/online', {socketId:socket.id})
.then((response)=>{
this.setState({username : username});

console.log(response);

socket.emit("new_user_online",{username:username});

socket.on("new_user_online",(data)=>{

  var onlineUsers = this.state.onlineUsers;
  console.log("new user online");
  onlineUsers.push(data.username);
  console.log(onlineUsers);
  console.log(data);
  this.setState({onlineUsers:onlineUsers});
  
  });
  
  socket.on('new_message', function (data) {
  console.log("---------------------new message");
  console.log(data);
    //  output.innerHTML += '<p><strong>' + data.sender + ': </strong>' + data.message + '</p>';
  
  }); 
  socket.on('load_chat_messages', function(data){
console.log(data);
  });
  // Listen for events
  socket.on('your_chat_name', function (data) {
  
  
      //currentChatName = data;
  
  }); 
  
  socket.on('typing', function (data) {
   //   feedback.innerHTML = '<p><em>' + data + ' is typing a message...</em></p>';
  });
  
  socket.on("online-users", (data)=>{
      console.log(data);
      this.setState({onlineUsers : data});
  });
})
.catch(function (error) {
console.log(error);
});

  });
}

  showChat(){
this.setState({Login : false, Register : false, Chat : true});
  }
showLogin(){
  this.setState({Login : true, Register : false, Chat : false} );

}
showRegister(){
  this.setState({Login : false, Register : true, Chat : false});

}

  render() {
   function  ComponentToDisplay(props){

      if(props.state.Login){
        return <Login_Register register={false} loggedIn={props.socketConnection}/>
      }
      else   if(props.state.Register){
        return <Login_Register register={true}/>
      }
      else{
        return <Chat socket={props.state.Socket} onlineUsers={props.state.onlineUsers}/>

      }
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Chat app</h1>
        </header>

  <NavBar showRegister = {this.showRegister} showLogin = {this.showLogin} showChat = {this.showChat}/>

 <ComponentToDisplay state ={this.state} socketConnection = {this.createSocketconnection}/>

      </div>
    );
  }
}

export default Layout;
