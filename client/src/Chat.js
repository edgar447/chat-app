import React, { Component } from 'react';
import openSocket from 'socket.io-client';
import axios from 'axios';
import OnlineUserList from "./OnlineUsersList";
//import "./ChatSocketEndpoint.js";
class Chat extends Component {

constructor(props){
super(props);
console.log(props);
this.sendMessage = this.sendMessage.bind(this);
this.handleChildClick = this.handleChildClick.bind(this);

}
state={
    count:0,
    socket:this.props.socket,
    authorized : false,
    onlineUsers:this.props.onlineUsers,
    render:false,
    username:"",
    companion:""
}

componentDidMount(){

//this.setState({socket : openSocket('http://localhost:3001')});



//var onlineUsers = [];
var rooms
var companion; // person you are chatting with right now
var currentChatName;
var roomdID;
axios.defaults.withCredentials = true;



axios.get('http://localhost:3001/chat')
.then((response)=>{
console.log(response);
if(response.status == 401){
    console.log("401");
}
else{
    this.setState({authorized : true, render:true})
   console.log(this.state);
}
})
.catch(function (error) {
    console.log(error);
  });

/*
// Listen for events
socket.on('update_online_users', function (data) {


    for (var i = 0; i < onlineUsers.length; i++) {
        console.log(onlineUsers[i] + onlineUsers.length);
    }

    if (onlineUsers.indexOf(data) == -1) {
        onlineUsers.push(data);
        var link = document.createElement("a");
    //    temp_link.href = "http://test.com";
        link.target = '_blank';
        link.innerHTML = data;


        var par = document.createElement("p");
        par.innerHTML = "";
        par.appendChild(link);

        users.appendChild(par);
        link.addEventListener('click', () => {
           
            var chat_participants = { sender: username, receiver: data };
            companion = data;

            this.state.socket.emit('new_chat', chat_participants);
        });
       // link.titel

       // users.innerHTML += '<p><a class = "online_users">' + data + '</a></p>';

    } 
});
*/
}

handleChildClick(onlineUser){
    this.setState({companion : onlineUser});
    console.log("pressed in chat");
    this.state.socket.emit("new_chat",{sender:this.state.username, receiver:onlineUser});
    console.log(onlineUser);
    }
    

 sendMessage(data){
    this.state.socket.emit('new_message', {
        message: "ffff",
        sender: this.state.username,
        private: true,
        receiver: this.state.companion
    });
  //  message.value = "";
  //  output.innerHTML += '<p><strong>' + username + ': </strong>' + data.message + '</p>';
  
}
  render() {
    return (
      <div>
{this.state.render

 ? <div id="chat">
            <h1>Chat</h1>
            <div id="chat-window">
                <div id="output"></div>
                <div id="feedback"></div>
            </div>
            <input id="handle" type="text"  />
            <input id="message" type="text" placeholder="Message" />
            <button onClick={this.sendMessage}>Send</button>
            <OnlineUserList userSelection={this.handleChildClick} onlineUsers={this.state.onlineUsers}></OnlineUserList>
        </div>
        :<div></div>
}
      </div>
    );
  }

}
export default Chat;
