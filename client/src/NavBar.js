import React, { Component } from 'react';
import "./Styles/NavBar.css"
class NavBar extends Component {
  render() {
    return (
      <div >
        <ul>
          <li onClick={this.props.showLogin}><a>Login</a></li>
          <li onClick={this.props.showRegister} ><a>Register</a></li>
          <li onClick={this.props.showChat}><a>Chat</a></li>
          <li ><a>Logout</a></li>
        </ul>
      </div>
    );
  }
}

export default NavBar;
