import React, { Component } from 'react';
import Message from './Message';
class MessageList extends Component {
constructor(props){
  super(props);
}

  render() {
    
    return (
      <div>
      {this.props.messages.map((x, index) => {
          return (
            <Message key={index} message={x.message} sender={x.sender} ></Message>

          );
        })}

      </div>
    );
  }
}

export default MessageList;
