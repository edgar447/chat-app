import React, { Component } from 'react';

class OnlineUser extends Component {
constructor(props){
  super(props);
  this.handleClick = this.handleClick.bind(this);
}
 handleClick() {
  this.props.userSelection(this.props.username);            

}
  render() {
    
    return (
      <div>

      <a onClick={this.handleClick}> {this.props.username}</a>

      </div>
    );
  }
}

export default OnlineUser;
