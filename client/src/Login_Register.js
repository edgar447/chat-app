import React, { Component } from 'react';
import axios from "axios";
class Login_Register extends Component {

constructor(props){
  super(props);

  this.handleSubmit = this.handleSubmit.bind(this);
  this.loggedIn = this.loggedIn.bind(this);
console.log(props);
}

componentDidMount(){
  axios.defaults.withCredentials = true;

}

  handleSubmit(){
    var username = document.querySelector("#username").value;
    var password = document.querySelector("#password").value;

if(this.props.register){
  this.handleRegistrationSubmit(username,password);
}
else{
  axios.post('http://localhost:3001/login', {
    username: username,
    password: password
  })
  .then( (response)=> {
    console.log(response);
    if(response.status === 200){
      console.log("200");
      this.loggedIn(username);
    }
  })
  .catch(function (error) {
    console.log(error);
  });}
  }

loggedIn(username){
  this.props.loggedIn(username);            
}

handleRegistrationSubmit(username, password){
  var passwordConfirmation = document.querySelector("#passwordConfirmation").value;

if(this.validateRegistrationData(username, password, passwordConfirmation)){
  axios.post('http://localhost:3001/register', {
    username: username,
    password: password,
    passwordConfirmation: passwordConfirmation
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}
}

validateRegistrationData(username, password,passwordConfirmation){

  var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
  if(format.test(username)){
return false;
  }
  if(format.test(password)){
    return false;
  }
  if(password !== passwordConfirmation){
return false;
  }
return true;
}

  render() {
    return (
      <div>


        <input id="username" name="username" type="text" placeholder="Your name..." />
        <input id="password" name="password" type="text" placeholder="Your password..." />
        {this.props.register
          ? <input id="passwordConfirmation" name="password" type="text" placeholder="Your password again..." />

          : <div></div>
        }
     
<button onClick ={this.handleSubmit}> Submit</button>

      </div>
    );
  }
}

export default Login_Register;
