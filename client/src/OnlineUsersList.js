import React, { Component } from 'react';
import OnlineUser from './OnlineUser';
class OnlineUsersList extends Component {
constructor(props){
  super(props);
  console.log(props);
  this.handleChildClick = this.handleChildClick.bind(this);

}

state={
  onlineUser:null
}

handleChildClick(onlineUser){
this.setState({onlineUserSelected : onlineUser});
console.log("pressed");
this.props.userSelection(onlineUser);            

console.log(onlineUser);
}

  render() {
    
    return (
      <div>

      {this.props.onlineUsers.map((x, index) => {
          return (
            <OnlineUser key={index} userSelection={this.handleChildClick} username={x} ></OnlineUser>

          );
        })}


      
      </div>
    );
  }
}

export default OnlineUsersList;
