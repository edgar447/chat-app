// BODY PARSER - -- 


var express = require('express');
var router = express.Router();
var User = require('./Database/user');
var bodyParser = require('body-parser');
var session = require('express-session');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json())
var MongoStore = require('connect-mongo')(session);
var OnlineUsers = require('./Database/online_users');

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
/*
// Login page 
router.get('/', function (req, res) {
    res.render('re', {
        users: users
    });
});
*/
/*
// Register page 
router.get('/', function (req, res) {
    console.log('hhh');
    err.status = 400;
    res.send("passwords dont match");
    return next(err);
});*/


//use sessions for tracking logins
router.use(session({
    secret: 'reis',
    resave: true,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }//,
  //  store: new MongoStore({
   //     mongooseConnection: db
 //   })
}));


// Register page 
router.post('/login', function (req, res, next) {
   
    if(req.body.username && req.body.password) {
        User.authenticate(req.body.username, req.body.password, function (error, user) {
          if (error || !user) {
            var err = new Error('Wrong email or password.');
            err.status = 401;
            return next(err);
          } else {
    
            req.session.userId = user._id;
            res.status = 200;
            return res.send();
          }
        });
      } else {
        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
      }
});

router.post('/register', function (req, res, next) {
    // confirm that user typed same password twice
    if (req.body.password !== req.body.passwordConfirmation) {
        var err = new Error('Passwords do not match.');
        err.status = 400;
        res.send("passwords dont match");
        return next(err);
    }

    if (req.body.username &&
        req.body.password &&
        req.body.passwordConfirmation) {

        var userData = {
            username: req.body.username,
            password: req.body.password,
            conversations : new Array()
        }

        User.create(userData, function (error, user) {
            if (error) {
                return next(error);
            } else {
                req.session.userId = user._id;

                res.status = 200;
            return res.send();
                }
        });

    } else {            

        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
    }

})

// GET route after registering
router.get('/profile', function (req, res, next) {
    User.findById(req.session.userId)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {
          if (user === null) {
            var err = new Error('Not authorized! Go back!');
            err.status = 400;
            return next(err);
          } else {
            return res.send('<h1>Name: </h1>' + user.username + '<h2>Mail: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
          }
        }
      });
  });
  
// GET route after registering
router.post('/online', function (req, res, next) {
    User.findById(req.session.userId)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {
          if (user === null) {
            var err = new Error('Not authorized! Go back!');
            err.status = 401;
            return next(err);
          } else {             

console.log("ready to update user");
 
        var ObjectId = require('mongodb').ObjectID;

        var query = { _id: ObjectId(req.session.userId) };
        console.log(" oooooooooooonline queeeeeeeeeeeeeeeeeeeeeeery");
        console.log(query);
        console.log('reseived socket id');
        console.log(req.body.socketId);
        var newvalues = { $set: { socketId: req.body.socketId } };
        User.updateOne(query, newvalues, function (err, res) { // Updating users socket.id
            if (err) throw err;
            console.log(res);
            console.log("1 document updated");
        });
            res.status = 200;
         return res.send();
    }
}
  });
});

  router.get('/', function (req, res, next) {
    if (req.session && req.session.userId) {
        res.redirect('')
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
});


// GET /logout
router.get('/logout', function (req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
});

function requiresLogin(req, res, next) {
    if (req.session && req.session.userId) {
        res.status = 200;
        next();
       // return res;
    } else {
        var err = new Error('You must be logged in to view this page.');
        err.status = 401;
        return next(err);
    }
}

router.get('/chat', requiresLogin, function (req, res, next) {
    User.findById(req.session.userId)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {

console.log(user);

       res.send({username:user.username});
        }
});
});





module.exports = router;