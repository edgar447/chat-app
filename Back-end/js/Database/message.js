var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({

    message: {
        type: String,
        required: true,
    },
    sender: {
        type: String,
        required: true,
    },
    conversationName: {
        type: String,
        required: true,
    }


});

MessageSchema.index({ conversationName: 1}); 

var Message = mongoose.model('Message', MessageSchema);
module.exports = Message;