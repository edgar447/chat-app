var mongoose = require('mongoose');

var ConversationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique:true
    },
    participant1:{
     name:{
         type:String,
         required:true
     },
     socketId:{
        type:String,
        required:false
     }
    },
    participant2:{
        name:{
            type:String,
            required:true
        },
        socketId:{
           type:String,
           required:false
        }
       }
   
   

});

var Conversation = mongoose.model('Conversation', ConversationSchema);
module.exports = Conversation;