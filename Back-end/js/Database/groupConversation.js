var mongoose = require('mongoose');

var GroupConversationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique:true
    },
    id: {
        type: String,
        required: false
    },
    members: {
        type: Array,
        required: true,
    },
    messageIds:{
        type:Array,
        required:false
    }

});

var GroupConversation = mongoose.model('GroupConversation', GroupConversationSchema);
module.exports = GroupConversation;