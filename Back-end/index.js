// BODY PARSER (APP.USE)- - - - - -JEIGU USERNAME IS NOT DEFINED
var UserConversation = require('./js/Database/Types/UserConversations');

var express = require('express');
var socket = require('socket.io');
var bodyParser = require('body-parser')
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var User = require('./js/Database/user');
var Conversations = require('./js/Database/conversation');
var Messages = require('./js/Database/message');

mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Database connection");
});

// Setting up an app
var app = express();

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// set the view engine to ejs
app.set('view engine', 'ejs');

//use sessions for tracking logins
app.use(session({
    secret: 'reis',
    resave: true,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    },
    store: new MongoStore({
        mongooseConnection: db
    })
}));


// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// include routes
var routes = require('./js/router');
app.use('/', routes);

// Static files
app.use(express.static(__dirname + '/css')); // Might be deleted in a future
app.use(express.static(__dirname + '/js'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('File Not Found');
    err.status = 404;
    next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send(err.message);
});

var server = app.listen(3001, function () {
    console.log('Listening for requests on port 3001,');
});



// Socket setup
var io = socket(server);



io.on('connection', (socket) => {
    console.log('New socket connection', socket.id);

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });

    socket.on('new_user_online', function (data) {
        console.log("new user online")
        console.log(data.username);
        console.log(socket.id);


        socket.broadcast.emit('new_user_online', { username: data.username });

    });

    // Handle chat event
    socket.on('new_message', function (message) {

        var user = findUserBySocketId(socket.id);

        for (var i = 0; i < sender.conversations.length; i++) {
            if (sender.conversations[i].companion.name === message.receiver) {
                if (!sender.conversations[i].companion.socketId) {
                    var receiver = findUserByName(message.receiver);

                    if (receiver) {

                        var messageContent = { sender: sender.username, message: message };
                        socket.to(receiver.socketId).emit("new_message", messageContent);
                        socket.emit("new_message", "EEEEEEEE");
                    }

}
                                console.log(sender.username);
var messageData = { message: message.message, sender: sender.username, conversationName: sender.conversations[i].name };

Messages.create(messageData, function (error, message) {
    if (error) {
        console.log(error);
        return;
    }
    console.log('new message added to DB');
});
                            }

                        }


                    }


                }
            });


    });
// Handle chat event    
socket.on('new_chat', function (data) {

    var user = findUserBySocketId(socket.id);
    var conversationName = user.username + "-" + data.receiver;
    var conversationExists = getMessages(user, conversationName, socket);
    if (!conversationExists) {
        newConversation(user, data.receiver, conversationName);
    }
});


// Handle typing event ----DOESNT WORK
socket.on('typing', function (data) {
    socket.broadcast.emit('typing', data); // Sends data to each socket excluding the on which sent the message
    console.log(data);
});

});

function findUserByName(username) {
    User.findOne({ username: username }).exec(function (error, user) { //Cheking if user exists
        if (error) {
            return next(error);
        }
        else if (!user) {
            error = new Error("User doesn't exists");
            error.status = 400;
            return next(error);
        } else {
            return user;
        }
    });
}

function newConversation(sender, receiverUsername, conversationName) {

    var receiver = findUserByName(receiverUsername);
    var conversationName = sender.username + "-" + receiver.username;
    createUserConversation(sender, receiver.username, null, conversationName);
    createUserConversation(receiver, sender.username, socket.id, conversationName);

}

function findUserBySocketId(socketId) {
    User.findOne({ socketId: socket.id })
        .exec(function (error, user) {
            if (error) {
                console.log(error);
                return next(error);
            } else
                if (user === null) {
                    console.log("unkown user")
                    var error = new Error('Unknow user');
                    error.status = 400;
                    return next(error);
                } else {
                    return user;
                }
        });
}

function createUserConversation(senderObject, receiverName, receiverSocketId, conversationName) {

    var conversationData = {
        name: conversationName,
        companion: { name: receiverName, socketId: receiverSocketId },
    };

    var senderConversations = senderObject.conversations;

    senderConversations.push(conversationData);

    //  var ObjectId = require('mongodb').ObjectID;
    //   var query = { _id: ObjectId(senderObject.userId) };
    var query = { username: senderObject.username };

    var newvalues = { $set: { conversations: senderConversations } };
    User.updateOne(query, newvalues, function (error, response) { // Updating users socket.id
        if (error) {
            console.log(error);
            throw error;
        } else {
            if (response.nModified === 1) {
                console.log("1 document updated");
            } else {
                console.log("object wasn't found")
            }
        }
    });
}

function getMessages(user, conversationName, socket) {

    for (var i = 0; i < user.conversations.length; i++) {
        console.log(user.conversations[i].name);
        if (user.conversations[i].name === conversationName) {
            Messages.find({ conversationName: conversationName }, function (error, messages) {
                if (error) {
                    console.log(error);
                }
                socket.emit("load_chat_messages", messages);

            });
            return true;
        }
    }
    return false;
}
